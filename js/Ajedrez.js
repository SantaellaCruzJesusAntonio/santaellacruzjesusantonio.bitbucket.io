var Ajedrez = (function () {
        /*Actualizacion de tablero
        -> obtenemos el elemnto cuyo id es msg
        -> preguntamos si el navegador esta online
            ->si si sobreescribimos el elemento hijo de tablero y agregamos una tabla con id tableroajedrez
            ejecutamos funcion mostrartablero
            ->si no
            escribimos en p del index fuerra de lina*/
        var _actualizar_tablero = function (){
        var mensaje= document.getElementById("msg");
        /*if(window.navigator.online){*/
            document.getElementById("tablero").innerHTML="<table id=\"tableroAjedrez\"></table>";
            this.mostrarTablero();
        /*}else{
            //mensaje.textContent="Fuera de linea";
        }*/};
    /*Mover una pza
        obtenemmos el p para escribir cualquier fallo que pueda sucitar
        validamos los datos de entrada mas no la syntaxis del objeto
            ->si son equivocaodos lanzamos mensaje en p de error
            ->si no recuperamos los elemntos del objeto con sus atributos asociados
            validamos que sean los valores <9 del tam�o del tablero de ajedrez, hacemos ajsute de las posiciones para transforma la letra a una posicion del arreglo y ser esa la que se modifique
            y se realiza el cambio de los valores asignados que tenia cada celda escribiendo en el html
    */
   var _mover_pieza = function (obj_parametros) {
        var mensaje = document.getElementById("msg");    
        if (obj_parametros.a === undefined && obj_parametros.de === undefined && obj_parametros.a === SyntaxError && obj_parametros.de === SyntaxError){
                mensaje.textContent = "Error en datos de movimiento";
            }
            var de=obj_parametros.de;
            var a=obj_parametros.a;
            
            if(de[1]<9 && a[1]<9){
                var aux = '&#934';
                var valor_de = document.getElementById("tableroAjedrez").rows[_convertir_posicion(de)].cells[de[1] - 1].innerText;
                var valor_a = document.getElementById("tableroAjedrez").rows[_convertir_posicion(a)].cells[a[1] - 1].innerText;
                 console.log(valor_a);   
                if (valor_a+"" =='\&#934'+""){
                    mensaje.textContent = "Lugar ocupado";                  
                }else{
                    console.log(valor_de);
                    console.log(valor_a);
                    document.getElementById("tableroAjedrez").rows[_convertir_posicion(de)].cells[de[1] - 1].innerHTML = valor_a;
                    document.getElementById("tableroAjedrez").rows[_convertir_posicion(a)].cells[a[1] - 1].innerHTML = valor_de;
                    mensaje.textContent = "Movimiento realizado";
                }
            }else{
                mensaje.textContent="Error en posiciones";
            }
            
    };
    /*funcion para traducir los valores de entrada(caracter) a valor numerico en posicion del arreglo*/
    var _convertir_posicion = function(valor){
        var mensaje= document.getElementById("msg");
          switch(valor[0]){
              case 'a': return 0;
              case 'b': return 1;
              case 'c': return 2;
              case 'd': return 3;
              case 'e': return 4;
              case 'f': return 5;
              case 'g': return 6;
              case 'h': return 7;
              default: return mensaje.textContent="posicion no encontrada";
          }
    };
    /*hace la peticion al servidor para descargar el archivo csv del cual toma la posici�n de las piezas 
    si hay un error lanza un mensaje 
    valido que la matriz que leemos es valida por el tama�o
        ojo-> No valida que solo existan los codigos para el ajedrez
    comvierto lo que leo en una cadena y mando a una funci�n que limpia de " , y \n
    ->dibuja las celdas especificando la clase a la que pertenece
        (esta clase especifica el color en el css)
        y agrega el simbolo correspondiente a la celda*/
    var _mostrar_tablero = function () {
        var mensaje = document.getElementById("msg");
        var servidor = "https://santaellacruzjesusantonio.bitbucket.io";
        var url = servidor + "/csv/tablero.csv" ;
                var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                var mensaje = document.getElementById("msg");
                var cadena_de_texto="";
                mensaje.textContent = "";
                if (this.status === 200) {
                    cadena_de_texto=this.response;
                    if(cadena_de_texto.length==256){
                        console.log("Lectura exitosa");
                        filas = document.getElementById('tableroAjedrez');
                        var filas;
                        var edo = 0;
                        var f = 0;
                        var c = 0;
                        var contador = 0;
                        console.log(cadena_de_texto);
                        var cadena=_limpia_cadena(cadena_de_texto);
                        for (f = 0; f <=7; f++) {
                            var fila = filas.insertRow();
                            for (c = 0; c <= 7; c++) {
                                var celda = fila.insertCell();
                                if (f % 2 == 0) {
                                    if (c % 2 == 0) {celda.innerHTML = '<span class=negras>' +_convierte_a_Unicode(cadena[contador]) + '</span>';}
                                    else {celda.innerHTML = '<span class=blancas>' + _convierte_a_Unicode(cadena[contador])+ '</span>';}
                                    contador++;
                                }
                                else {
                                    if (c % 2 == 0){celda.innerHTML = '<span class=blancas>' + _convierte_a_Unicode(cadena[contador])+ '</span>';}
                                    else {celda.innerHTML = '<span class=negras>' + _convierte_a_Unicode(cadena[contador]) + '</span>';}
                                    contador++;
                                }
                            }
                        }
                    }else{
                        mensaje.textContent = "Error en la lectura de archivo";   
                    }
                } else {
                    mensaje.textContent = "Error " + this.status + " " + this.statusText
                        + " - " + this.responseURL;
                }
            }
        };
        xhr.open('GET', url, true);
        xhr.send();
    };
    /*funcion que recibe una cadena de caracteres y regresa una cadena sin " , y saltos de lina o { }*/
    var _limpia_cadena=function(cadena){
        var aux="";
        for(var i=0;i<cadena.length;i++){
            if (cadena[i] != '"' && cadena[i] != ',' && cadena[i] != '\n' && cadena[i] != '{' && cadena[i] != '}'){
                aux=aux+cadena[i];
            }
        }
        return aux;
    };
    /*convierte las letras lehidas en su simbolo en unicode*/
    var _convierte_a_Unicode= function(caracter){
        switch(caracter){
            case "t": return "&#9820";
            case "c": return "&#9822";
            case "a": return "&#9821";
            case "r": return "&#9819";
            case "y": return "&#9818";
            case "p": return "&#9823";
            case "T": return "&#9814";
            case "C": return "&#9816";
            case "A": return "&#9815";
            case "R": return "&#9813";
            case "Y": return "&#9812";
            case "P": return "&#9817";
            default: return "&#934" ;


        }
    };
return {
    'mostrarTablero': _mostrar_tablero,
    'actualizarTablero': _actualizar_tablero,
    'moverPieza':_mover_pieza
    };
}) ();